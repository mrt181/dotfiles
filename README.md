# dotfiles

<!--toc:start-->
- [dotfiles](#dotfiles)
  - [Requirements](#requirements)
    - [Git](#git)
    - [Fish](#fish)
    - [Shell](#shell)
    - [fzf](#fzf)
    - [Fisher](#fisher)
    - [Fisher plugins](#fisher-plugins)
    - [Cascadia font](#cascadia-font)
    - [yq](#yq)
      - [Linux](#linux)
      - [WSL](#wsl)
    - [Tmux](#tmux)
    - [Delta](#delta)
    - [Lazygit](#lazygit)
    - [Rust](#rust)
    - [Cargo binstall](#cargo-binstall)
    - [Cargo cargo-update](#cargo-cargo-update)
    - [Starship](#starship)
    - [Ripgrep](#ripgrep)
    - [Lsd](#lsd)
    - [Bat](#bat)
    - [Fd-find](#fd-find)
    - [vivid](#vivid)
    - [yazi](#yazi)
    - [Nvm](#nvm)
    - [Tree-sitter-cli](#tree-sitter-cli)
    - [Neovim](#neovim)
    - [Stow](#stow)
  - [Installation](#installation)
    - [LazyVim](#lazyvim)
  - [Windows](#windows)
  - [Theme](#theme)
    - [Catppuccin](#catppuccin)
      - [Terminal](#terminal)
<!--toc:end-->

dotfiles to setup a shell environment on a new machine.

## Requirements

Install the following.

### Git

```bash
sudo apt install git
```

Clone your dotfiles repo.

```bash
git clone https://gitlab.com/mrt181/dotfiles .dotfiles \
;cd .dotfiles
```

### Fish

```bash
sudo apt-add-repository ppa:fish-shell/release-3 \
;sudo apt update \
;sudo apt install fish
```

### Shell

```bash
chsh -s /usr/bin/fish
```

### fzf

```bash
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf

~/.fzf/install
```

### Fisher

```bash
curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish | source \
;fisher install jorgebucaran/fisher
```

### Fisher plugins

```bash
fisher install jorgebucaran/nvm.fish \
;fisher install patrickf1/fzf.fish \
;fisher install patrickf1/colored_man_pages.fish
```

### Cascadia font

```bash
set cascadiaversion 2404.23
curl -LO https://github.com/microsoft/cascadia-code/releases/download/v$cascadiaversion/CascadiaCode-$cascadiaversion.zip
```

### yq

#### Linux

```bash
sudo snap intall yq \
mkdir -p ~/.local/bin/ \
;ln -s /snap/yq/current/bin/yq ~/.local/bin/yq
```

#### WSL

```bash
sudo wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/bin/yq \
;sudo chmod +x /usr/bin/yq
```

### Tmux

```bash
sudo apt install tmux \
;git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```

### Delta

```bash
curl -LO https://github.com/dandavison/delta/releases/download/0.17.0/git-delta_0.17.0_amd64.deb \
;sudo dpkg -i git-delta_0.17.0_amd64.deb 

delta --generate-completion fish >> ~/.config/fish/completions/delta.fish
```

### Lazygit

```bash
set LAZYGIT_VERSION (curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | grep -Po '"tag_name": "v\K[^"]*') \
;curl -Lo lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/latest/download/lazygit_"$LAZYGIT_VERSION"_Linux_x86_64.tar.gz" \
;tar xf lazygit.tar.gz lazygit \
;sudo install lazygit ~/bin
```

### Rust

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

### Cargo binstall

```bash
curl -L --proto '=https' --tlsv1.2 -sSf https://raw.githubusercontent.com/cargo-bins/cargo-binstall/main/install-from-binstall-release.sh | bash \
;cargo binstall cargo-binstall
```

### Cargo cargo-update

```bash
cargo binstall cargo-update -y
```

### Starship

```bash
cargo binstall starship -y
```

### Ripgrep

```bash
cargo binstall ripgrep -y
```

### Lsd

```bash
cargo binstall lsd -y
```

### Bat

```bash
cargo binstall bat -y
```

### Fd-find

```bash
cargo binstall fd-find -y
```

### vivid

```bash
cargo binstall vivid -y
```

### yazi

```bash
cargo binstall yazi-fm yazi-cli
```

### Nvm

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash \
;nvm install lts

npm install -g npm
```

### Tree-sitter-cli

```bash
npm install -g tree-sitter-cli
```

### Neovim

```bash
sudo snap install nvim --classic \
;sudo update-alternatives --install /usr/bin/editor editor /snap/bin/nvim 60 \
;sudo snap alias nvim editor
```

### Stow

```bash
sudo apt install stow
```

## Installation

Run stow to create symlinks

```bash
rm ~/.config/fish/config.fish

stow -v -R .
```

Prepare bat

```bash
bat cache -b
```

### LazyVim

see: [LazyVim](https://www.lazyvim.org/installation)

## Windows

Enable WSL in "Turn Windows features on or off".

```ps
wsl --install -d Ubuntu
```

Open Settings and edit the Ubuntu profile.

Set `Command line` to

```ps
ubuntu.exe run tmux new
````

Set `Starting directory` to

```text
\\wsl$\<distro-name>\home\<distro-user-name>
```

## Theme

### Catppuccin

#### Terminal

see: [Catppuccin Windows Terminal](https://github.com/catppuccin/windows-terminal)
Place the content of this file `https://raw.githubusercontent.com/catppuccin/windows-terminal/main/mocha.json` in Terminal's `settings.json` in the schemes array.
Place the content of this file `https://raw.githubusercontent.com/catppuccin/windows-terminal/main/mochaTheme.json` in Terminal's `settings.json` in the themes array - create it if it is missing.
