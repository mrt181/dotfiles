if status is-interactive
    fzf_configure_bindings --processes=\ep --git_log=\el --git_status=\es
    fish_vi_key_bindings
    fish_add_path $HOME/.rvm/bin/
    source (~/.cargo/bin/starship init fish --print-full-init | psub)
    set -gx LS_COLORS $(vivid generate catppuccin-mocha)
    nvm use -s lts
end
