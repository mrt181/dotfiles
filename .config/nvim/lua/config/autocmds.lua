-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua
-- Add any additional autocmds here
vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead", "BufEnter" }, {
  group = vim.api.nvim_create_augroup("filetype_props", { clear = true }),
  desc = "Change filetype to xml",
  callback = function()
    if vim.fn.expand("%:e") == "props" then
      vim.bo.filetype = "xml"
    end
  end,
})
